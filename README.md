# Home Removal App Front End #

git clone the repo

$ cd homeremovals

Download dev deps using npm, postInstall runs bower for the front end deps.

$ npm install

To serve the app use (by default serves on 8000): 

$ python -m SimpleHttpServer

The app will be available from:

localhost:8000/app/index.html

Grunt is used to copy, concat, ng-template run tests and jshint. From root dir run the below:

$ grunt

Tests are also runnable from jasmine spec runner: localhost:8000/test/tests.html

localhost:8000/test/tests.html


## App Summary ##

In the customer entry form all Fields are required so the add button will only be enabled if all fields have values. Summary of items are displayed in an accordian. Click the accordian title to display the item list.


In the remover manifest data is stored in the shared item-storage-service. So each refresh of the app will clear this cache. Manifest view is available with location #/remover or clicking on the 'remover manifest' button.
