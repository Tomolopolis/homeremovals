module.exports = function(config) {
    'use strict';

    config.set({
        browsers: ['PhantomJS'],
        frameworks: ['jasmine'],
        reporters: ['progress', 'junit'],
        junit: {
            outputFile: 'build/test-results/home-removals-client-test-results.xml'
        },
        files: [
            'bower_components/underscore/underscore.js',
            'bower_components/angular/angular.js',
            'bower_components/angular-mocks/angular-mocks.js',
            'bower_components/angular-route/angular-route.js',
            'bower_components/angular-bootstrap/ui-bootstrap.js',
            'bower_components/angular-bootstrap/ui-bootstrap-tpls.js',
            'app/**/*-module.js',
            'app/**/*-ctrl.js',
            'app/**/*-service.js',
            'test/spec/**/*.js'
        ]
    })
}