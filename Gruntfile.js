module.exports = function(grunt) {
  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    jshint: {
      files: ['app/**/*.js'],
      options: {
        jshintrc: '.jshintrc'
      }
    },
    karma: {
      single: {
        configFile: 'karma.conf.js',
        singleRun: true
      }
    },
    copy: {
      dist: {
        files: [ {
          expand: true,
          cwd: 'app',
          src: ['index.html'],
          dest: 'dist'
        }]
      }
    },
    useminPrepare: {
      html: 'app/index.html',
      options: {
        dest:'dist',
        flow: {
          steps: {
            'js': ['concat'],
            'css': ['concat']
          },
          post: {}
        }
      }
    },
    ngtemplates: {
      'home-removals': {
        cwd: 'app',
        src: ['**/*.html', '!index.html'],
        dest: '.tmp/templates.js',
        options: {
          standalone: false,
          concat: 'generated'
        }
      }
    },
    usemin: {
      html: 'dist/index.html'
    },
    less: {
        dist: {
            files: {
              'styles/app.css': 'styles/main.less'
          }
        }
    },
    watch: {
        files: 'styles/*.less',
        tasks: ['less']
    }
  });

  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-karma');
  grunt.loadNpmTasks('grunt-usemin');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-angular-templates');
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-watch');

  grunt.registerTask('build', ['less', 'useminPrepare', 'ngtemplates', 'copy', 'concat', 'usemin']);
  grunt.registerTask('test', ['jshint', 'karma']);
  grunt.registerTask('default', ['build', 'test']);
  // Default task(s).
  //grunt.registerTask('default', ['watch']);

};