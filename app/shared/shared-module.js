(function(ng) {
    'use strict';

    var module = ng.module('homeRemovals.shared', []);

    module.run(['$log', function($log) {
        $log.debug('Initialised shared module');
    }]);
})(angular);

