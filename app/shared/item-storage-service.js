(function(ng) {
    'use strict';

    var module = ng.module('homeRemovals.shared');

    var serviceId = 'ItemStorageService';

    module.service(serviceId, ['$log', service]);

    /**
    * This service essentially delegates to a cache instance. Realistically, this would
    * hold all the logic to make calls to a backend api to store / retrieve items.
    */
    function service($log) {

        var cache = [];
        var counter = 0;

        var ItemStorageService = function() {
            $log.debug('Service debugged');
        };

        ItemStorageService.prototype.addItem = function(item) {
            item.id = ++counter;
            cache.push(item);
            return cache;
        };

        ItemStorageService.prototype.removeItem = function(item) {
            if (item.id) {
                cache = cache.filter(function(cachedItem) {
                    return cachedItem.id !== item.id;
                });
                return cache;
            } else {
                $log.warn('failed to remove an item with no ID');
            }
        };

        ItemStorageService.prototype.retrieveItems = function() {
            return cache;
        };

        return new ItemStorageService();
    }   

})(angular);

