(function(ng) {
    'use strict';

    var module = ng.module('homeRemovals.removalSummary');

    var controllerId = 'RemovalSummaryCtrl';

    module.controller(controllerId, ['$scope', '$log', 'ItemStorageService', controller]);

    function controller($scope, $log, itemService) {

        var items = itemService.retrieveItems();
        
        var sortedItems = _.sortBy(items, function(item) {
            return item.weight;
        });

        $scope.fragileItems = sortedItems.filter(function(item) {
            return item.fragile;
        });

        var groupedItems = _.groupBy(sortedItems, function(item) {
            return item.room;
        });

        var topGroups = {};
        var leftOverItems = [];
        _.each(groupedItems, function(value, key) {
            topGroups[key] = [value.pop(), value.pop()].filter(function(item) {
                return item;
            });
            leftOverItems = leftOverItems.concat(value.filter(function(item) {
                return !item.fragile;
            }));
        });

        $scope.topGroups = topGroups;
        $scope.leftOverItems = leftOverItems;
    }

})(angular);

