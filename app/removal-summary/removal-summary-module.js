(function(ng) {
    'use strict';

    var module = ng.module('homeRemovals.removalSummary', []);

    module.run(['$log', function($log) {
        $log.debug('Initialised mover entry module');
    }]);
})(angular);

