(function(ng, _) {
    'use strict';

    var module = ng.module('homeRemovals.customerEntry');

    var controllerId = 'CustomerEntryCtrl';

    module.controller(controllerId, ['$scope', '$log', '$location', 'ItemStorageService', controller]);

    function controller($scope, $log, $location, itemService) {
        $scope.roomOptions = [
            { id: 'Kitchen', label: 'Kitchen' },
            { id: 'livingRoom', label: 'Living Room / Reception Room' },
            { id: 'Bedroom', label: 'Bedroom' },
            { id: 'Bathroom', label: 'Bathroom' },
            { id: 'Other', label: 'Other' }
        ];

        $scope.item = {};
        $scope.freshItem = true;
        $scope.items = itemService.retrieveItems();

        $scope.addItem = function() {
            $scope.items = itemService.addItem($scope.item);
            $scope.item = {};
            $scope.freshItem = true;
        };

        $scope.removeItem = function(item) {
            $scope.items = itemService.removeItem(item);
        };

        $scope.modifyItem = function(item) {
            $scope.item = item;
            $scope.items = itemService.removeItem(item);
            $scope.freshItem = false;
        };

        $scope.showAccordionHeading = function(roomType) {
            return  _.findWhere($scope.items, { room: roomType.id });
        };

        $scope.itemsForRoom = function(roomType) {
            return $scope.items.filter(function(item) {
                return item.room === roomType.id;
            });
        };

        $scope.viewManifest = function() {
            $location.path('/remover');
        };
    }
})(angular, _);

