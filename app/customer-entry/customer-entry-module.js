(function(ng) {
    'use strict';

    var module = ng.module('homeRemovals.customerEntry', ['ui.bootstrap']);

    module.run(['$log', function($log) {
        $log.debug('Initialised customery entry module');
    }]);
})(angular);

