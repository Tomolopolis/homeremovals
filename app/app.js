(function(ng) {
    'use strict';

    var module = ng.module('HomeRemovals', ['ngRoute', 'homeRemovals.customerEntry', 'homeRemovals.removalSummary', 'homeRemovals.shared']);

    module.config(['$routeProvider', defineRoutes]);

    function defineRoutes($routeProvider) {
        $routeProvider
                .when('/', { templateUrl: 'customer-entry/customer-entry.html' })
                .when('/remover', { templateUrl: 'removal-summary/removal-summary.html' });
    }
})(angular);

