/* jshint globalstrict: true -W117 */
'use-strict';

describe('RemovalSummaryCtrl', function() {

    var scope;
    var controllerId = 'RemovalSummaryCtrl';
    var itemService = {
        retrieveItems: function(item) {
        }
    };

    var items = [
        {id:1, description: 'Big Box', weight: 100, room: 'livingRoom'},
        {id:2, description: 'Small Box', weight: 10, room: 'Bedroom'},
        {id:3, description: 'Medium Box', weight: 50, room: 'Bathroom'},
        {id:5, description: 'Big Box', weight: 1.5, room: 'livingRoom', fragile: true},
        {id:6, description: 'leftOver Box 1', weight: 1.5, room: 'livingRoom', fragile: false},
        {id:4, description: 'Box', weight: 10, room: 'livingRoom'},
        {id:7, description: 'leftOver Box 2', weight: 1.5, room: 'livingRoom', fragile: false}
    ];

    beforeEach(angular.mock.module('homeRemovals.removalSummary', function($provide) {
        $provide.value('ItemStorageService', itemService);
    }));

    beforeEach(angular.mock.inject(function($rootScope, $injector, $controller) {
        scope = $rootScope.$new();

        spyOn(itemService, 'retrieveItems').and.returnValue(items);

        $controller(controllerId, {
            $scope: scope
        });
    }));

    describe('scope', function() {

        it('should be initialised with fragile items', function() {
            expect(scope.fragileItems).toEqual([
                {id:5, description: 'Big Box', weight: 1.5, room: 'livingRoom', fragile: true}
            ]);
        });

        it('should be intialised with heaviest items grouped by room', function() {
            expect(scope.topGroups).toEqual({
                'livingRoom': [
                    {id:1, description: 'Big Box', weight: 100, room: 'livingRoom'},
                    {id:4, description: 'Box', weight: 10, room: 'livingRoom'},
                ],
                'Bedroom': [
                    {id:2, description: 'Small Box', weight: 10, room: 'Bedroom'},
                ],
                'Bathroom': [
                    {id:3, description: 'Medium Box', weight: 50, room: 'Bathroom'},
                ]
            });
        });

        it('should be intialised with items that are not the heaviest for a room and not fragile', function(){
            expect(scope.leftOverItems).toEqual([
                {id:6, description: 'leftOver Box 1', weight: 1.5, room: 'livingRoom', fragile: false},
                {id:7, description: 'leftOver Box 2', weight: 1.5, room: 'livingRoom', fragile: false}
            ]);
        })
    });
})

