/* jshint globalstrict: true -W117 */
'use-strict';

describe('CustomerEntryCtrl', function() {

    var scope;
    var controllerId = 'CustomerEntryCtrl';
    var itemService = {
        addItem: function(item) {
        },
        removeItem: function(item) {
        },
        retrieveItems: function() {
        }
    };

    var item = { 
        description: 'Big Box', 
        weight: 100, 
        room: 'livingRoom'
    };

    beforeEach(angular.mock.module('homeRemovals.customerEntry', function($provide) {
        $provide.value('ItemStorageService', itemService);
    }));

    beforeEach(angular.mock.inject(function($rootScope, $injector, $controller) {
        scope = $rootScope.$new();

        spyOn(itemService, 'addItem').and.returnValue([item]);
        spyOn(itemService, 'removeItem').and.returnValue([]);
        spyOn(itemService, 'retrieveItems').and.returnValue([]);

        $controller(controllerId, {
            $scope: scope
        });
    }));

    describe('scope', function() {

        it('should be initialised fresh with blank item and empty item list', function() {
            expect(scope.item).toEqual({});
            expect(scope.freshItem).toEqual(true);
            expect(scope.items).toEqual([]);
        });

        it('should be intialised with roomOptions', function() {
             var roomOptions = [
                { id: 'Kitchen', label: 'Kitchen' },
                { id: 'livingRoom', label: 'Living Room / Reception Room' },
                { id: 'Bedroom', label: 'Bedroom' },
                { id: 'Bathroom', label: 'Bathroom' },
                { id: 'Other', label: 'Other' }
            ];

            expect(scope.roomOptions).toEqual(roomOptions);
        });
    });

    describe('item interactions', function() {

        it('should add an item and intialise fresh item', function() {

            scope.item = item;
            scope.addItem(item);

            expect(itemService.addItem.calls.count()).toEqual(1);
            expect(itemService.addItem.calls.argsFor(0)).toEqual([item]);
            expect(scope.items).toEqual([item]);
            expect(scope.freshItem).toEqual(true);
        });

        it('should modify items and not be fresh', function() {
            
            scope.items = ['test'];
            scope.item = 'test';

            scope.modifyItem(item);

            expect(scope.item).toEqual(item);
            expect(itemService.removeItem.calls.count()).toEqual(1);
            expect(itemService.removeItem.calls.argsFor(0)).toEqual([item]);
            expect(scope.items).toEqual([]);
            expect(scope.freshItem).toEqual(false);
        })
    });
})

