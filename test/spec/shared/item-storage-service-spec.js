/* jshint globalstrict: true -W117 */
'use-strict';

describe('RemovalSummaryCtrl', function() {

    var service;
    var item = { 
        description: 'Big Box', 
        weight: 100, 
        room: 'livingRoom'
    };

    beforeEach(angular.mock.module('homeRemovals.shared'));

    beforeEach(angular.mock.inject(function(ItemStorageService) {
        service = ItemStorageService;
    }));

    describe('service', function() {
        it('should add items', function() { 
            expect(service.addItem(item)).toEqual([{ 
                    id: 1,
                    description: 'Big Box', 
                    weight: 100, 
                    room: 'livingRoom'
                }
            ]);
        });

        it('should remove items', function() {
            service.addItem(item);

            expect(service.removeItem(item)).toEqual([]);
        });
    });
})

